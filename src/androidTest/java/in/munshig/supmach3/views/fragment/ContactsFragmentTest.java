package in.munshig.supmach3.views.fragment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContactsFragmentTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getLifecycle() {
    }

    @Test
    public void getViewLifecycleOwner() {
    }

    @Test
    public void getViewLifecycleOwnerLiveData() {
    }

    @Test
    public void getViewModelStore() {
    }

    @Test
    public void getDefaultViewModelProviderFactory() {
    }

    @Test
    public void getSavedStateRegistry() {
    }

    @Test
    public void instantiate() {
    }

    @Test
    public void instantiate1() {
    }

    @Test
    public void restoreViewState() {
    }

    @Test
    public void isInBackStack() {
    }

    @Test
    public void equals1() {
    }

    @Test
    public void hashCode1() {
    }

    @Test
    public void toString1() {
    }

    @Test
    public void getId() {
    }

    @Test
    public void getTag() {
    }

    @Test
    public void setArguments() {
    }

    @Test
    public void getArguments() {
    }

    @Test
    public void requireArguments() {
    }

    @Test
    public void isStateSaved() {
    }

    @Test
    public void setInitialSavedState() {
    }

    @Test
    public void setTargetFragment() {
    }

    @Test
    public void getTargetFragment() {
    }

    @Test
    public void getTargetRequestCode() {
    }

    @Test
    public void getContext() {
    }

    @Test
    public void requireContext() {
    }

    @Test
    public void getActivity() {
    }

    @Test
    public void requireActivity() {
    }

    @Test
    public void getHost() {
    }

    @Test
    public void requireHost() {
    }

    @Test
    public void getResources() {
    }

    @Test
    public void getText() {
    }

    @Test
    public void getString() {
    }

    @Test
    public void getString1() {
    }

    @Test
    public void getFragmentManager() {
    }

    @Test
    public void requireFragmentManager() {
    }

    @Test
    public void getChildFragmentManager() {
    }

    @Test
    public void getParentFragment() {
    }

    @Test
    public void requireParentFragment() {
    }

    @Test
    public void isAdded() {
    }

    @Test
    public void isDetached() {
    }

    @Test
    public void isRemoving() {
    }

    @Test
    public void isRemovingParent() {
    }

    @Test
    public void isInLayout() {
    }

    @Test
    public void isResumed() {
    }

    @Test
    public void isVisible() {
    }

    @Test
    public void isHidden() {
    }

    @Test
    public void hasOptionsMenu() {
    }

    @Test
    public void isMenuVisible() {
    }

    @Test
    public void onHiddenChanged() {
    }

    @Test
    public void setRetainInstance() {
    }

    @Test
    public void getRetainInstance() {
    }

    @Test
    public void setHasOptionsMenu() {
    }

    @Test
    public void setMenuVisibility() {
    }

    @Test
    public void setUserVisibleHint() {
    }

    @Test
    public void getUserVisibleHint() {
    }

    @Test
    public void getLoaderManager() {
    }

    @Test
    public void startActivity() {
    }

    @Test
    public void startActivity1() {
    }

    @Test
    public void performCreateOptionsMenu() {
    }

    @Test
    public void performPrepareOptionsMenu() {
    }

    @Test
    public void performOptionsItemSelected() {
    }

    @Test
    public void performContextItemSelected() {
    }

    @Test
    public void performOptionsMenuClosed() {
    }

    @Test
    public void performSaveInstanceState() {
    }

    @Test
    public void performPause() {
    }

    @Test
    public void performStop() {
    }

    @Test
    public void performDestroyView() {
    }

    @Test
    public void performDestroy() {
    }

    @Test
    public void performDetach() {
    }

    @Test
    public void setOnStartEnterTransitionListener() {
    }

    @Test
    public void getNextAnim() {
    }

    @Test
    public void setNextAnim() {
    }

    @Test
    public void getNextTransition() {
    }

    @Test
    public void setNextTransition() {
    }

    @Test
    public void getEnterTransitionCallback() {
    }

    @Test
    public void getExitTransitionCallback() {
    }

    @Test
    public void getAnimatingAway() {
    }

    @Test
    public void setAnimatingAway() {
    }

    @Test
    public void setAnimator() {
    }

    @Test
    public void getAnimator() {
    }

    @Test
    public void getStateAfterAnimating() {
    }

    @Test
    public void setStateAfterAnimating() {
    }

    @Test
    public void isPostponed() {
    }

    @Test
    public void isHideReplaced() {
    }

    @Test
    public void setHideReplaced() {
    }

    @Test
    public void newInstance() {
    }

    @Test
    public void onCreateView() {
    }

    @Test
    public void onViewCreated() {
    }
}
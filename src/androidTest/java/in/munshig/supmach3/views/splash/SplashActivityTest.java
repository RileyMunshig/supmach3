package in.munshig.supmach3.views.splash;

import android.content.res.Resources;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.collection.SimpleArrayMap;
import androidx.collection.SparseArrayCompat;
import androidx.core.app.ComponentActivity;
import androidx.fragment.app.FragmentController;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.reactivestreams.Subscription;

import io.reactivex.Observer;

import static org.mockito.Mockito.*;

public class SplashActivityTest {
    @Mock
    Subscription subscription;
    @Mock
    SplashNavigator splashNavigator;
    @Mock
    Observer observer;
    @Mock
    AppCompatDelegate mDelegate;
    @Mock
    Resources mResources;
    @Mock
    FragmentController mFragments;
    @Mock
    LifecycleRegistry mFragmentLifecycleRegistry;
    @Mock
    SparseArrayCompat<String> mPendingFragmentActivityResults;
    @Mock
    LifecycleRegistry mLifecycleRegistry;
    //Field mSavedStateRegistryController of type SavedStateRegistryController - was not mocked since Mockito doesn't mock a Final class when 'mock-maker-inline' option is not set
    @Mock
    ViewModelStore mViewModelStore;
    @Mock
    ViewModelProvider.Factory mDefaultFactory;
    //Field mOnBackPressedDispatcher of type OnBackPressedDispatcher - was not mocked since Mockito doesn't mock a Final class when 'mock-maker-inline' option is not set
    @Mock
    SimpleArrayMap<Class<? extends ComponentActivity.ExtraData>, ComponentActivity.ExtraData> mExtraDataMap;
    @InjectMocks
    SplashActivity splashActivity;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnCreate() throws Exception {
        splashActivity.onCreate(null);
    }

    @Test
    public void testOnBackPressed() throws Exception {
        splashActivity.onBackPressed();
    }

    @Test
    public void testOnPause() throws Exception {
        splashActivity.onPause();
    }

    @Test
    public void testOnResume() throws Exception {
        splashActivity.onResume();
    }

    @Test
    public void testOpenLoginActivity() throws Exception {
        splashActivity.openLoginActivity();
    }

    @Test
    public void testOpenMainActivity() throws Exception {
        splashActivity.openMainActivity();
    }

    @Test
    public void testOnPointerCaptureChanged() throws Exception {
        splashActivity.onPointerCaptureChanged(true);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
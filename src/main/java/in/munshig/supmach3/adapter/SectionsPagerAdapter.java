package in.munshig.supmach3.adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import org.jetbrains.annotations.NotNull;
import in.munshig.supmach3.R;
import in.munshig.supmach3.views.fragment.ContactsFragment;
import in.munshig.supmach3.views.fragment.ProfileFragment;
import in.munshig.supmach3.views.fragment.LogFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    @StringRes
    private static final int[] TAB_TITLES = new int[] { R.string.Isights, R.string.logs,R.string.contacts  };
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;}
    @NotNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return ProfileFragment.newInstance();
        }else  if (position == 1) {
            return LogFragment.newInstance();
        }
        else {
            return ContactsFragment.newInstance();
        }
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }
    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }
}
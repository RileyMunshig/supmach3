package in.munshig.supmach3.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.futuremind.recyclerviewfastscroll.SectionTitleProvider;

import java.util.Collections;
import java.util.List;

import in.munshig.supmach3.R;
import in.munshig.supmach3.views.modals.User;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> implements SectionTitleProvider {

    Context context;
    OnItemClickListener mListener;
    List<User> data = Collections.emptyList();


    public UserAdapter(Context context, List<User> user, OnItemClickListener listener) {
        this.context = context;
        this.mListener = listener;
        this.data = user;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.udhaar_card, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        User user = data.get(position);
        holder.bind(user);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private String getName(int position) {
        return data.get(position).getName();
    }

    @Override
    public String getSectionTitle(int position) {
        return getName(position).substring(0, 1);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView contactIcon, nameTV, numberTV, Amount, symbol, amountStatus;

        public MyViewHolder(View view) {
            super(view);
            contactIcon = view.findViewById(R.id.contactIconTV);
            nameTV = view.findViewById(R.id.nameTV);
            numberTV = view.findViewById(R.id.numberTV);
            Amount = view.findViewById(R.id.rupeeSymbolTV);
            symbol = view.findViewById(R.id.balanceTV);
            amountStatus = view.findViewById(R.id.amountStatusTV);
        }

        public void bind(User user) {
            double amount = user.getNetBalance();

            if (user.getName() != null)
                contactIcon.setText(user.getName().subSequence(0, 1));
            nameTV.setText(user.getName());
            Log.i("CheckNumber", " || number || " + user.getNumber());
            //Setting up user number :)
            if (user.getNumber() != null) {
                numberTV.setVisibility(View.VISIBLE);
                numberTV.setText(user.getNumber());
            } else {
                numberTV.setVisibility(View.GONE);
            }
            //Setting up account amount status :D
            if (amount > 0) {
                Amount.setText(String.valueOf(user.getNetBalance()));
                amountStatus.setText(context.getResources().getString(R.string.udhaarToPay));
                amountStatus.setTextColor(context.getResources().getColor(R.color.colorGreen));
            } else if (amount < 0) {
                Amount.setText(String.valueOf(-user.getNetBalance()));
                amountStatus.setText(context.getResources().getString(R.string.udhaarToReceive));
                amountStatus.setTextColor(context.getResources().getColor(R.color.colorRed));
            } else {
                Amount.setText(String.valueOf(user.getNetBalance()));
                amountStatus.setText(context.getResources().getString(R.string.udhaarAllClear));
                amountStatus.setTextColor(context.getResources().getColor(R.color.grey));
            }

            itemView.setOnClickListener(v -> {
                mListener.onItemClick(user);
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(User user);
    }
}

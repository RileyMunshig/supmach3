package in.munshig.supmach3.base;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Config {
    private SharedPreferences sharedPreferences;
    public String storesPath;
    public final static String STORE_ID = "storeID";
    public static int APP_MODE_RETAILER = 1;
    public static int APP_MODE_BUSINESS = 2;
    public Config(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        storesPath = versionPath + "/stores/" + sharedPreferences.getString(STORE_ID, null) + "/";
    }
    public final static String versionPath = "v2",
            humansPath = "humans/",
            billsPath = "bills/",
            salesPath = "sales",
            paymentsPath = "payments/",
            aamSupplierId = "a",
            vendorsPath = versionPath + "/" + "vendors/",
            profilePath = "profile/",
            trash = "trash/",
            trashSales = trash + salesPath,
            trashPayments = trash + paymentsPath,
            trashHumans = trash + humansPath,
            trashTransactions = trash + billsPath;

    //type of transaction
    public final static int TYPE_PAYMENT = 1;
    public final static int TYPE_SALE = 2;
    public final static int TYPE_PURCHASE = 3;

    //payment mode types
    public final static String PAYMENT_MODE_CASH = "Cash";
    public final static String PAYMENT_MODE_CHEQUE = "Cheque";
    public final static String PAYMENT_MODE_NET_BANKING = "Net Banking";
    public final static String PAYMENT_MODE_UPI = "UPI";

    public final static String IMAGE_PAYMENT = "payments";
    public final static String IMAGE_BILL = "bills";


}

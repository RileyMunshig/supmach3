package in.munshig.supmach3.base;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateConverters {
    //24 hours in milisecond;
    public static long DAY_IN_MILISECOND = 86400000;
    /*
     * Function to get the 13 digit Timestamp from the string of format yyyy MM dd HH:mm:ss
     * @param dateForm   string of date in the format yyyy MM dd HH:mm:ss
     * @return   a long value, that is a 13 digit timestamp
     */
    public static long getTimeStampFromDate(String dateForm) {
        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            Date mDate = sdf.parse(dateForm);
            assert mDate != null;
            timeInMilliseconds = mDate.getTime();
            Log.i("Date in milli :: ", String.valueOf(timeInMilliseconds));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public static long getTimeStamp(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Date mDate = simpleDateFormat.parse(date);
            return mDate.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /*
     * Function to get today's date using Calendar class
     * @return a string of format yyyy mm dd
     */
    public static String get_Current_date() {
        //set current date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String curr_date = mDay + "-" + mMonth + "-" + mYear;
        Log.i("current date", curr_date);
        return curr_date;
    }

    /*
    Function to get date in the format used from the timestamp
     */
    public static String TimestamptoDate(long timestamp) {
        Date date = new Date(timestamp);
        return new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(date);
    }

    public static String getDateTimeFromTS(long timestamp) {
        Date date = new Date(timestamp);
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH).format(date);
    }

    public static String getDate(long timestamp) {
        Date date = new Date(timestamp);
        return new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(date);
    }

    public static long getCurrentTimeStamp() {
        Date date = new Date();
        return date.getTime();
    }

    public static long getTodayStartTimeStamp() {
        String startDate = get_Current_date();
        return getTimeStampFromDate(startDate + " 00:00:00");
    }

    public static String getDateTimeFromTimeStampIn12HrFormat(long timestamp) {
        Date date = new Date(timestamp);
        String twelveHrFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.ENGLISH).format(date);
        String[] splitted = twelveHrFormat.split(" ");
        return splitted[0] + ",  " + splitted[1] + " " + splitted[2];
    }

    public static String get12HrTime(long timestamp) {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        Calendar calToday = Calendar.getInstance();
        calToday.setTimeInMillis(timestamp);
        return dateFormat.format(calToday.getTime());
    }
}

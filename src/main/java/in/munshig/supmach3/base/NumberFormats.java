package in.munshig.supmach3.base;

import java.util.Locale;

public class NumberFormats {
    /*
    This method removes the trailing zeroes.
    @param  takes a string as an input
    @return     returns a formatted string with no trailing zero
     */
    public static String removeTrailingZeroes(String s)
    {
        double d = Double.parseDouble(s);
        return removeTrailingZeroes(d);
    }

    public static String removeTrailingZeroes(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);//TODO:Buggy
        else
            return String.format("%s",d);
    }

    public static String removeTrailingZeroes(Double d)
    {
        double k = d;
        return removeTrailingZeroes(k);
    }

    public static String getDebitString(int amount) {
        switch (amount) {
            case 1:
                return "₹";
            case 2:
                return "₹₹";
            case 3:
            default:
                return "₹₹₹";
        }
    }
}

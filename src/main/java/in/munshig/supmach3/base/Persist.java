package in.munshig.supmach3.base;

import android.app.Application;
import android.content.pm.PackageManager;
import android.content.res.Configuration;

import com.franmontiel.localechanger.LocaleChanger;


import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Persist extends Application {
    public static final List<Locale> SUPPORTED_LOCALES =
            Arrays.asList(
                    new Locale("en", "US"),
                    new Locale("hi", "IN"),
                    new Locale("hing", "IN"),
                    new Locale("ml", "IN"));
    @Override
    public void onCreate() {
        super.onCreate();
       // ButterKnife.setDebug(BuildConfig.DEBUG);//TODO:Add build config to Test Application persist
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        LocaleChanger.initialize(getApplicationContext(), SUPPORTED_LOCALES);
    }

    @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }
    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageGids(packagename);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}

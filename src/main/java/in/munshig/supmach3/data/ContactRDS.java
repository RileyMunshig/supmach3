package in.munshig.supmach3.data;

import android.app.Application;
import android.util.Log;


import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import in.munshig.supmach3.utils.Pref;
import in.munshig.supmach3.views.modals.Profile;
import in.munshig.supmach3.views.modals.User;


public class ContactRDS {
    private final String TAG = "ContactRDS";
    private static ContactRDS INSTANCE;
    private CollectionReference contactsRef, userRef, tempRef;
    private String myUid;
    private Pref prefManager;
    private FirebaseFirestore db;
    private LiveData<List<User>> liveContactsData;

    public static ContactRDS getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ContactRDS(application);
        }
        return INSTANCE;



    }

    private ContactRDS(Application application) {
        db = FirebaseFirestore.getInstance();
        String root = "Users";
        myUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userRef = db.collection(root);
        contactsRef = db.collection(root).document(myUid).collection("contacts");
        prefManager = new Pref(application.getApplicationContext());
    }

    public void createContact(User user)
    {
        contactsRef.document(user.getUid()).set(user)
                .addOnSuccessListener(aVoid -> Log.i(TAG, "createContact || successful"));
    }

    public LiveData<List<User>> getLiveContactsData() {
        return liveContactsData;
    }

    public void addContactToContact(User user)
    {
        String name = "Anonymous";
        Profile profile = prefManager.getProfile();
        User me  = new User();
        if(!profile.getUserName().equals(""))
            name = profile.getUserName();
        me.setName(name);
        me.setNumber(profile.getNumber());
        me.setUid(profile.getuId());
        me.setChatRef(user.getChatRef());
        me.setCreated((new Date()).getTime());
        me.setModified((new Date()).getTime());

        userRef.document(user.getUid()).collection("contacts").document(myUid).set(me)
                .addOnSuccessListener(aVoid ->
                        {
                            Log.i(TAG, "addContactToContact || successful");
                            writeToTemp("Users/"+user.getUid()+"/contacts/"+myUid, user.getUid());
                        }

                );
    }

    private void writeToTemp(String path, String receiverId)
    {
        CollectionReference userRef = db.collection("Users");
        userRef.document(receiverId).get().addOnCompleteListener(task -> {
//                Log.i(TAG, "writeToTemp() || task.getResult()= " + task.getResult());
//                Log.i(TAG, "writeToTemp() || task.getResult().getdata= " + task.getResult().getData());
            if(task.isSuccessful() && task.getResult() != null && task.getResult().getData() != null)
            {
                HashMap<String, String> detail = new HashMap<>();
                detail.put("status", "added");
                detail.put("path", path);
                detail.put("entity", "contacts");

                String ts = String.valueOf((new Date()).getTime());
                HashMap<String, Object> map = new HashMap<>();
                map.put(ts, detail);
                tempRef = db.collection("temp");
                tempRef.document(receiverId).set(map, SetOptions.merge());
            }
        });
    }

    public void updateNetBalance(User user) {
        contactsRef.document(user.getUid()).set(user).addOnSuccessListener(aVoid -> { });
    }


    public  interface onSucess{


    }

/*    public MutableLiveData<ArrayList<Contacts>> getContactData(){
        loadContactData();
        MutableLiveData<ArrayList<Contacts>> data = new MutableLiveData<>();
        data.setValue(arrayList);
        return  data;
    }

    private void loadContactData() {
        db.collection("Users").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                if(!queryDocumentSnapshots.isEmpty()){

                    List<DocumentSnapshot>list = queryDocumentSnapshots.getDocuments();
                    for(DocumentSnapshot documentSnapshot : list){

                        arrayList.add(documentSnapshot.toObject(Contacts.class));

                    }
                    Log.d(TAG, "onSuccess: added");
                    onDataAdded.added();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Log.d(TAG, "onFailure: ");

            }
        });
    }*/



}
package in.munshig.supmach3.utils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public final class AnimUtils {

    private AnimUtils() { }

    /** Landing Page Animation**/
    public static void scaleAnimateView(View view) {
        ScaleAnimation animation =
                new ScaleAnimation(
                        1.15f, 1, 1.15f, 1,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

        view.setAnimation(animation);
        animation.setDuration(100);
        animation.start();
    }

    /** Splash Animation**/
    public  static  void translateAnimation( View view){
        TranslateAnimation translateAnimation =
                new TranslateAnimation(
                        1.15f, 1, 1.15f, 1);
        view.setAnimation(translateAnimation);
        translateAnimation.setDuration(100);
        translateAnimation.start();
    }
}

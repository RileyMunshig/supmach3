package in.munshig.supmach3.utils;

public interface Constants {
    String TAG = "ConstantsTag";
    /**
     * SignIn
     */
    int RC_SIGN_IN = 123;

    /**
     * Fragments
     */

    /**
     * Activities|Settings
     */
    String PRIVACY_POLICY = "http://www.munshig.in";
    String TOS_URL = "http://www.munshig.in";

    String ACTION_MY_ACCOUNT = "MyAccount";
    String ACTION_SETTINGS = "Setting";
    String ACTION_SHARE = "ShareApp";
    String ACTION_HELP_CENTER = "Help";
    String ACTION_ABOUT_US = "About";
    String ACTION_WHATSAPP = "About";

    /**
     * Firebase
     */
    String  USERS = "Users";
    String  TRANSACTIONS = "Ledgers";



    /**
     * Responses
     */
    public static final String STATUS_CODE_FAILED = "failed";

    public static final String STATUS_CODE_SUCCESS = "success";

    /**
     * Date|Time
     */

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";



    /**
     * Trasactions and Contacts Utils
     */
    String UDHAAR_INTENT_KEY = "user";
    String PASSWORD_REF = "PASSWORD_REF" ;
    String CONTACTS_ = "Contacts";


}


package in.munshig.supmach3.utils;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
public class Dinosaur {

    private static Dinosaur mInstance;

    public static Dinosaur getInstance() {
        if (mInstance == null) {
            mInstance = new Dinosaur();
        }
        return mInstance;
    }

    private Dinosaur() {
    }

    private PublishSubject<String> publisher = PublishSubject.create();

    public void publish(String event) {
        publisher.onNext(event);
    }

    // Listen should return an Observable
    public Observable<String> listen() {
        return publisher;
    }
}
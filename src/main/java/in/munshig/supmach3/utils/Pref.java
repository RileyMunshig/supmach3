package in.munshig.supmach3.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import in.munshig.supmach3.views.modals.Profile;


public class Pref {
    private static final String TAG = "Prefs";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private static final String PREF_NAME = "Welcome";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public static int LANGUAGE_ENGLISH = 0;
    public static int LANGUAGE_HINDI = 1;
    public static int LANGUAGE_HINGLISH = 2;
    public static int LANGUAGE_MALAYALAM = 3;


    public Pref(Context context) {
        this._context = context;
        int PRIVATE_MODE = 0;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setProfile(Profile user) {
        if (user == null)
            return; // don't bother

        Log.i(TAG, "setProfile = " + user.getuId());
        Log.i(TAG, "setProfile = " + user.getUserName());

        editor.putString("uId", user.getuId());
        editor.putString("userName", user.getUserName());
        editor.putString("shopName", user.getShopName());
        editor.putString("number", user.getNumber());
        editor.putString("address", user.getAddress());
        editor.putString("email", user.getEmail());
        editor.commit();
    }

    /**
     * Retrieve
     */
    public Profile getProfile() {
        String uid = pref.getString("uId", "");
        String userName = pref.getString("userName", "");
        String shopName = pref.getString("shopName", "");
        String number = pref.getString("number", "");
        String address = pref.getString("address", "");
        String email = pref.getString("email", "");

        Log.i(TAG, uid + " | " + userName + " | " + shopName + " | " + number + " | " + address + " | " + email);

        return new Profile(uid, userName, shopName, number, address, email);
    }

    public void setAppMmode(int appMode) {
        SharedPreferences.Editor editor = pref.edit();
        try {
            editor.putInt("appMode", appMode);
        } catch (Exception e) {
            Log.i(TAG, "Saving app mode records" + e.toString());
        }
        editor.apply();
    }

    public int getAppMode(){
        return pref.getInt("appMode", 1);
    }

    public void setCurrentLanguage(int currentLanguage) {
        SharedPreferences.Editor editor = pref.edit();
        try {
            editor.putInt("setCurrentLanguage", currentLanguage);
        } catch (Exception e) {
            Log.i(TAG, "Saving current language " + e.toString());
        }
        editor.apply();
    }

    public int getCurrentLanguage() {
        return pref.getInt("setCurrentLanguage", 0);
    }

}
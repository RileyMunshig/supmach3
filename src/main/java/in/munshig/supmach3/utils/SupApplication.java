package in.munshig.supmach3.utils;

import android.app.Application;
import android.content.Intent;
import android.view.View;

import dagger.android.AndroidInjector;
import dagger.android.HasAndroidInjector;
import in.munshig.supmach3.BuildConfig;
import in.munshig.supmach3.views.activities.HomeActivity;
import timber.log.Timber;

public class SupApplication extends Application implements HasAndroidInjector {
        @Override
        public void onCreate() {
            super.onCreate();
            if (BuildConfig.DEBUG) {
                Timber.plant(new Timber.DebugTree());
            } else {
                Timber.plant(new Release());
            }
        }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return null;
    }


    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
            v.setAlpha(1f);
        }
    }
    public void hideViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }
    public void showViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }
    public void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
            v.setAlpha(0.5f);
            v.setClickable(false);
            v.setFocusable(false);
        }
    }

    public void goHome() {
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }
}





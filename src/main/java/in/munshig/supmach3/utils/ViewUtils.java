package in.munshig.supmach3.utils;
import android.content.Intent;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;

import in.munshig.supmach3.views.activities.HomeActivity;

public class ViewUtils extends  AppCompatActivity  {

    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
            v.setAlpha(1f);
        }
    }
    public void hideViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }
    public void showViews(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }
    public void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
            v.setAlpha(0.5f);
            v.setClickable(false);
            v.setFocusable(false);
        }
    }

    public void goHome() {
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }

}

package in.munshig.supmach3.viewmodals;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import in.munshig.supmach3.views.contacts.ContactsRepo;
import in.munshig.supmach3.views.modals.User;

public class AddContactVM extends AndroidViewModel {
    private String TAG = "AddContactVM";
    private ContactsRepo mRepository;
    private String name = "", number = "";
    private List<User> contactData;
    private String codeSent;

    public AddContactVM(Application application) {
        super(application);
        mRepository = ContactsRepo.getInstance(application);
    }

    public String getCodeSent() {
        return codeSent;
    }

    public void setCodeSent(String codeSent) {
        this.codeSent = codeSent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null)
            this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        if (number != null)
            this.number = number;
    }

    public LiveData<List<User>> getContactsList() {
        return mRepository.getContacts();
    }

    private boolean searchUser(String ph)
    {
        Log.i(TAG, "searchUser || ph " + ph);
        for(User user : contactData)
        {
            Log.i(TAG, "searchUser || user.getNumber() " + user.getNumber());
            if(user.getNumber()!=null && user.getNumber().trim().equals(ph))
            {
                return true;
            }
        }
        return false;
    }

    public boolean insert() {
        if (number.equals("")) {
            //always a new contact
            mRepository.insert(name);
            return true;
        } else {
            //check if the phone number exists for any user in the sup contact book of user
            boolean found = searchUser(number);
            Log.i(TAG, "insert || found" + found);
            if(found)
                return false;
            else {
                mRepository.insert(name, number);
                return true;
            }
        }
    }

    public void setContactsData(List<User> users) {
        contactData = users;
    }
}

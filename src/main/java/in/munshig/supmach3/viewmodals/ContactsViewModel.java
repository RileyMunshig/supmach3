package in.munshig.supmach3.viewmodals;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import in.munshig.supmach3.data.ContactRDS;
import in.munshig.supmach3.views.modals.User;


public class ContactsViewModel extends AndroidViewModel {

    private String TAG = "ContactsViewModel";
    private ContactRDS contactRDS;

    public ContactsViewModel(Application application) {
        super(application);
      //  contactLDS = ContactLDS.getInstance(application);
        contactRDS = ContactRDS.getInstance(application);
    }
    public LiveData<List<User>> getUserData(){
        Log.d(TAG, "getUserData: " + contactRDS.getLiveContactsData());
        return  contactRDS.getLiveContactsData();
    }



}

package in.munshig.supmach3.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.munshig.supmach3.R;
import in.munshig.supmach3.adapter.SectionsPagerAdapter;
import in.munshig.supmach3.utils.SupApplication;
import in.munshig.supmach3.utils.ViewUtils;
import in.munshig.supmach3.views.landing.LandingPage;
import timber.log.Timber;

public class HomeActivity extends ViewUtils {
    @BindView(R.id.mtabs)
    TabLayout home_tabLayout;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.mcalc)
    FloatingActionButton calcFloat;
   /* @BindView(R.id.addContact) FloatingActionButton contactFloat;*/
    @BindView(R.id.appBar)
    AppBarLayout mAppbar;

    @BindView(R.id.mtoolbar)
    Toolbar m_toolbar;
    TabLayout tabs;
    AppBarLayout.Behavior behavior;
    CoordinatorLayout rootLayout;
    private int[] tabIcons = {R.drawable.munshi_logo};
    Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.d("onCreate");
        super.onCreate(savedInstanceState);
       // new SetTheme(this);x
        setContentView(R.layout.activity_home);
        Timber.d("HomeActivity");
        unbinder = ButterKnife.bind(this);
        SectionsPagerAdapter sectionsPagerAdapter =
                new SectionsPagerAdapter(this, getSupportFragmentManager());
        mViewPager.setAdapter(sectionsPagerAdapter);
        home_tabLayout.setupWithViewPager(mViewPager);
        m_toolbar.setTitle("SUPMach3");
        setupTabIcons();

        home_tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    m_toolbar.setTitle("Insights");
                   hideViews(calcFloat/*contactFloat*/);
                } else if (tab.getPosition() == 1) {
                    m_toolbar.setTitle("Logs");
                    showViews(calcFloat);
                    /*hideViews(contactFloat);*/
                } else if(tab.getPosition() == 2){
                    m_toolbar.setTitle("Contacts");
                   /* showViews(contactFloat);*/
                    hideViews(calcFloat);
                }
                else{
                    m_toolbar.setTitle("SUPMach3");
                    /*hideViews(calcFloat,contactFloat);*/
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
    private void setupTabIcons() {
        home_tabLayout.getTabAt(0).setIcon(tabIcons[0]);
    }

    public void onFirstRun() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);
        Timber.i("getCheck", " || home activity boolean || " + isFirstRun);
        if (isFirstRun) {
            Timber.i("getCheck", " || Home activity if First time   || " + isFirstRun);
            startActivity(new Intent(HomeActivity.this, LandingPage.class));
            Toast.makeText(HomeActivity.this, "Welcome",Toast.LENGTH_LONG)
                    .show();
            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                    .putBoolean("isFirstRun", false).apply();
        }
    }
    public void collapseToolbar(){
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)mAppbar.getLayoutParams();
        behavior = (AppBarLayout.Behavior) params.getBehavior();
        if(behavior!=null) {
            behavior.onNestedFling(rootLayout, mAppbar, null, 0, 10000, true);
        }
    }


/*    @OnClick({R.id.addContact})
    public void setLoadAmount(View view){
        switch(view.getId()){
            case R.id.addContact:
                addContact();
                break;
    
        }
    }*/

    private void addContact() {
    }

    @Override
    protected void onStart() {
        super.onStart();
        onFirstRun();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Timber.d("onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
    }

    @Override
    public void onStateNotSaved() {
        super.onStateNotSaved();
        Timber.d("onStateNotSaved");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Timber.d("onRestart");
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}

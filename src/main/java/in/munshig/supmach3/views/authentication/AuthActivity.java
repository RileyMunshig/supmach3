package in.munshig.supmach3.views.authentication;
import com.stfalcon.androidmvvmhelper.mvvm.activities.BindingActivity;
import in.munshig.supmach3.R;


public class AuthActivity extends BindingActivity{

    @Override
    public AuthActivityVM onCreate() {
        return new AuthActivityVM(this);
    }
    @Override
    public int getVariable() {
        return 0;
    }
    @Override
    public int getLayoutId() {
        return R.layout.activity_auth;
    }

}
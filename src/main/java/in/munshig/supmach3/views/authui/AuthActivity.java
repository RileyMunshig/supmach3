package in.munshig.supmach3.views.authui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Collections;
import in.munshig.supmach3.R;
import in.munshig.supmach3.base.BaseActivity;
import in.munshig.supmach3.utils.ViewUtils;
import in.munshig.supmach3.views.activities.HomeActivity;
import timber.log.Timber;
import static in.munshig.supmach3.utils.Constants.PRIVACY_POLICY;
import static in.munshig.supmach3.utils.Constants.RC_SIGN_IN;
import static in.munshig.supmach3.utils.Constants.TOS_URL;
public class AuthActivity extends ViewUtils {
    private final String TAG = AuthActivity.class.getSimpleName();
    public Boolean flag = false;
    FirebaseAuth mAuth;
    //Change Language Method :D
/*    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_auth);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(AuthActivity.this, HomeActivity.class));
        }else{
            resultCall();
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.i("onActivityResult || requestcode= " + requestCode + " || RC_SIGN_IN= " + RC_SIGN_IN);
        if (requestCode == RC_SIGN_IN) {
            Timber.i("onActivityResult || resultCode= " + resultCode + " || RESULT_OK= " + RESULT_OK);
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK ) {
                saveProfile();
                startActivity(new Intent(AuthActivity.this, HomeActivity.class));
                finish();
                return;
            } else {
                if (response == null) {
                    ShowAlertBox("Login canceled");
                    return;
                }
                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    ShowAlertBox("No Internet Connection");
                    return;
                }
                if (response.getError().getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    ShowAlertBox("Unknown Error");
                    return;
                }
            }
            ShowAlertBox("Unknown sign in response");
        }
    }
    private void resultCall() {
        flag = true;
        Log.i(TAG, "resultCall");
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(Collections.singletonList(
                                new AuthUI.IdpConfig.PhoneBuilder().build()
                        ))
                        .setTheme(R.style.AppTheme)
                        .setLogo(R.mipmap.ic_launcher)
                        .setTosUrl(TOS_URL)
                        .setPrivacyPolicyUrl(PRIVACY_POLICY)
                        .build(),
                RC_SIGN_IN);
    }

    protected void ShowAlertBox(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(AuthActivity.this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Ok",
                (dialog, id) -> dialog.cancel());
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void saveProfile() {

    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart:AddContact ");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: ");
    }
    public void onRetry(View view) {
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}

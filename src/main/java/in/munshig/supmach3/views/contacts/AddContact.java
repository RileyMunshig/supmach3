package in.munshig.supmach3.views.contacts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.franmontiel.localechanger.LocaleChanger;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.munshig.supmach3.R;
import in.munshig.supmach3.viewmodals.AddContactVM;


public class AddContact extends AppCompatActivity implements View.OnClickListener {
    public static String CONTACT_NAME = "contactName";
    public static String CONTACT_NUMBER = "contactNumber";
    private String TAG = "AddContact.class";
    private AddContactVM addContactVM;

    @BindView(R.id.my_toolbar)
    Toolbar toolbar;

    @BindView(R.id.saveBtn)
    Button saveButton;

    //Contact name Edit text :D
    @BindView(R.id.contactNameEdt)
    EditText nameEdt;

    //Contact Number Edit Text :D
    @BindView(R.id.contactNumberEdt)
    EditText numberEdt;


    //Change Language Method :D
/*    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        Log.d(TAG, "AddContact: ");
        ButterKnife.bind(this);
        activateListener();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(v->finish());

       // toolbar.setNavigationOnClickListener(v -> onBackPressed());
        addContactVM = new AddContactVM(getApplication());
        addContactVM.getContactsList().observe(this, users -> addContactVM.setContactsData(users));

        addContactVM.setNumber("");
        addContactVM.setName("");

        Intent intent = getIntent();
        if (intent != null) {
            addContactVM.setName(intent.getStringExtra(CONTACT_NAME));
            Log.i(TAG, " |Contact name | " + addContactVM.getName());
            addContactVM.setNumber(intent.getStringExtra(CONTACT_NUMBER));
            Log.i(TAG, " |Contact number | " + addContactVM.getNumber());
        }

        nameEdt.setText(addContactVM.getName().trim());
        String num = addContactVM.getNumber().trim();
        num = num.replaceAll(" ","");
        num = num.replace("+91", "");
        num = num.replaceAll("-", "");
        numberEdt.setText(num.trim());
    }

    // --------------------- [ activate listener ] -----------------------------------------------\\
    private void activateListener() {
        saveButton.setOnClickListener(this);
    }

    // --------------------- [ onClick viw method :D ]--------------------------------------------\\
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.saveBtn) {
            addContactVM.setName(nameEdt.getText().toString().trim());
            addContactVM.setNumber(numberEdt.getText().toString().trim());
            boolean validated = addContactVM.insert();
            if(validated){

                View contextView = findViewById(R.id.contactview);
                Snackbar snackbar = Snackbar
                        .make(contextView, "Contact Added Sucessfully", Snackbar.LENGTH_LONG)
                        .setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Snackbar snackbar1 = Snackbar.make(contextView, "Message is restored!", Snackbar.LENGTH_SHORT);
                                snackbar1.show();
                            }
                        });

                snackbar.show();
                finish();
            }
            else{
                View contextView = findViewById(R.id.contactview);
                Snackbar snackbar1 = Snackbar.make(contextView, "Contact Already Exists!", Snackbar.LENGTH_SHORT);
                snackbar1.show();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart:AddContact ");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: ");
    }



}

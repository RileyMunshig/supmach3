package in.munshig.supmach3.views.contacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.futuremind.recyclerviewfastscroll.SectionTitleProvider;
import java.util.List;
import in.munshig.supmach3.R;
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> implements SectionTitleProvider {
    private Context context;
    private List<Contact> contactList;
    OnItemClickListener mListener;

    ContactAdapter(Context context, List<Contact> contactList, OnItemClickListener listener) {
        this.context = context;
        this.contactList = contactList;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_card, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact con = contactList.get(position);
        holder.bind(con);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    private String getName(int position) {
        return contactList.get(position).getName();
    }


    @Override
    public String getSectionTitle(int position) {
        //this String will be shown in a bubble for specified position
        return getName(position).substring(0, 1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, phone_number;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.nameTV);
            phone_number = view.findViewById(R.id.numberTV);
        }

        public void bind(Contact con) {
            name.setText(con.getName());
            phone_number.setText(con.getPhone_number());
            itemView.setOnClickListener(v -> {
                mListener.onItemClick(con);
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Contact con);
    }
}

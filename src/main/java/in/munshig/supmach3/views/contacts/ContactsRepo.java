package in.munshig.supmach3.views.contacts;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Date;
import java.util.List;

import in.munshig.supmach3.data.ContactRDS;
import in.munshig.supmach3.views.modals.User;


public class ContactsRepo {

    private final String TAG = "ContactsRepo";
    private static ContactsRepo INSTANCE;
    private ContactRDS contactRDS;
    private CollectionReference contactsRef;
    public static ContactsRepo getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ContactsRepo(application);
        }
        return INSTANCE;
    }
    private ContactsRepo(Application application) {
        contactRDS = ContactRDS.getInstance(application);
        String root = "Users";
        String myUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        contactsRef = db.collection(root);
    }

    public LiveData<List<User>> getContacts() {
        return contactRDS.getLiveContactsData();
    }

    public void insert(String name) {
        FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
        DocumentReference ref = FirebaseFirestore.getInstance().collection("Users").document(fUser.getUid()).collection("Contacts").document();
        String cId = ref.getId();
        User user = new User(cId, name);

        createContact(user);
    }

    private void createContact(User user)
    {
        //get the id of the new doc in the transactions collection and save doc id in the chat ref of user
        DocumentReference ref = FirebaseFirestore.getInstance().collection("Ledgers").document();
        String chatId = ref.getId();

        user.setChatRef(chatId);
        user.setCreated((new Date()).getTime());
        user.setModified((new Date()).getTime());
        contactRDS.createContact(user);
    }

    private void createContactWhenNumberIsAdded(User user)
    {
        createContact(user);
        contactRDS.addContactToContact(user);
    }

    public void insert(String name, String number) {

        contactsRef.whereEqualTo("number", "+91" + number).get().addOnCompleteListener(task -> {
            Log.i(TAG, "task.getResult()= " + task.getResult());
            if(task.isSuccessful() && task.getResult()!= null)
                querySnapshotFound(task.getResult(), name, number);
        });
    }

    public void querySnapshotFound(QuerySnapshot q, String name, String number) {
        if(q.getDocuments().size() > 0)
        {
            //user found
            //get the user info
           // Log.i(TAG, "querySnapshotFound || q.getDocuments()= " + q.getDocuments());
            Log.i(TAG, "querySnapshotFound || q.getDocuments().size= " + q.getDocuments().size());
            for(DocumentSnapshot doc : q.getDocuments())
            {
                Log.i(TAG, "querySnapshotFound || doc= " + doc.getData());

                String uid = doc.getData().get("uId").toString();
                String uNumber = doc.getData().get("number").toString();
                User makeUser = new User(uid, name, uNumber);

                createContactWhenNumberIsAdded(makeUser);
            }
        }
        else{
            //no such user
            //create new user
            Log.i(TAG, "querySnapshotFound || not found");

            FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
            DocumentReference ref = FirebaseFirestore.getInstance().collection("Users").document(fUser.getUid()).collection("Contacts").document();
            String cId = ref.getId();
            User user = new User(cId, name, number);

            createContact(user);
        }
    }


}

package in.munshig.supmach3.views.contacts;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.franmontiel.localechanger.LocaleChanger;
import com.futuremind.recyclerviewfastscroll.FastScroller;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.munshig.supmach3.R;
import in.munshig.supmach3.base.BaseActivity;
import in.munshig.supmach3.utils.ViewUtils;


public class CreateContact extends ViewUtils implements ContactAdapter.OnItemClickListener {
    private static final String TAG = CreateContact.class.getSimpleName();
    public static final String EXTRA_INTENT = "EXTRA_INTENT";
    //Recycler  View :D
    @BindView(R.id.contactRV)
    RecyclerView recyclerView;

    //Fast scroll View :)
    @BindView(R.id.fastScroll)
    FastScroller fastScroller;

    //Add new Contact Layout
    @BindView(R.id.addNewContactLayout)
    RelativeLayout newContactLayout;

    private List<Contact> contactList = new ArrayList<>();
    private ContactAdapter mAdapter;
    private static final int PERMISSION_REQUEST_CONTACT = 1000;
    Unbinder unbinder;

    //Change Language Method :D
 /*   @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }*/

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        unbinder = ButterKnife.bind(this);
        mAdapter = new ContactAdapter(CreateContact.this, contactList, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        // To show item divider, uncomment below line
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        fastScroller.setRecyclerView(recyclerView);

        newContactLayout.setOnClickListener(v -> {
            startActivity(new Intent(CreateContact.this, AddContact.class));
        });

        checkPermission();
    }

    //------------------------[ On back action bar back button ] ---------------------------------\\
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void prepareData() {
        contactList.clear();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (phones != null) {
            while (phones.moveToNext()) {
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contactList.add(new Contact(name, phoneNumber));
            }
        }
        phones.close();

        if (contactList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            //emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            //emptyView.setVisibility(View.GONE);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    prepareData();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateContact.this);
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");
                    builder.setOnDismissListener(dialog -> checkPermission());
                    builder.show();
                }
                return;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_CONTACTS
                    },
                    PERMISSION_REQUEST_CONTACT);
        } else {
            prepareData();
        }
    }

    @Override
    public void onItemClick(Contact con) {
        Log.i("GetContacts", " || " + con.getName());
        Intent intent = new Intent(CreateContact.this, AddContact.class);
        intent.putExtra(AddContact.CONTACT_NAME, con.getName());
        intent.putExtra(AddContact.CONTACT_NUMBER, con.getPhone_number());
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart:AddContact ");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}



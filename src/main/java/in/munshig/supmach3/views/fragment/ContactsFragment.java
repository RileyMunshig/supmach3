package in.munshig.supmach3.views.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.munshig.supmach3.R;
import in.munshig.supmach3.adapter.UserAdapter;
import in.munshig.supmach3.utils.Constants;
import in.munshig.supmach3.utils.InputUtil;
import in.munshig.supmach3.viewmodals.ContactsViewModel;
import in.munshig.supmach3.views.contacts.CreateContact;
import in.munshig.supmach3.views.modals.User;

public class ContactsFragment extends Fragment implements UserAdapter.OnItemClickListener{
    private String TAG = "UdhaarFragment";
    private ContactsViewModel mContactsViewModel;
    private Toolbar toolbar;
    //Floating action button :D
    @BindView(R.id.add_contact)
    FloatingActionButton addContactFloat;
    //Recycler View
    @BindView(R.id.udhaarRV)RecyclerView recyclerView;
    ProgressDialog mProgressDialog;
    /* UserAdapter userAdapter = new UserAdapter();*/
    /*SearchView*/
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    public ContactsFragment() {
    }

    public static ContactsFragment newInstance() {
        return new ContactsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContactsViewModel = new ContactsViewModel(Objects.requireNonNull(getActivity()).getApplication());

    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view1 = inflater.inflate(R.layout.fragment_contacts, container, false);
        ButterKnife.bind(this, view1);
        // mProgressDialog = view1.findViewById(R.id.progress_dialog);
        InputUtil.hideKeyboard(Objects.requireNonNull(getContext()), view1);
        setHasOptionsMenu(true);
        mContactsViewModel = new ContactsViewModel((getActivity()).getApplication());
        FloatingActionButton fab = view1.findViewById(R.id.add_contact);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), CreateContact.class);//AddNewContact
            startActivity(intent);
        });
        return view1;
    }


    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(true);
            mProgressDialog.setMessage("Loading...............");
            mProgressDialog = new ProgressDialog(getContext(), R.style.CustomDialog);
            mProgressDialog.setTitle("Fetching Your data in");
        }
        mProgressDialog.show();

    }
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }



    public void onItemClick(User user) {
     /*   Intent intent = new Intent(getActivity(), UdhaarSummary.class);
        intent.putExtra(Constants.UDHAAR_INTENT_KEY, user.getUid());
        startActivity(intent);
        Log.d(TAG, "onItemClick: ");*/
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                InputMethodManager mImm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mImm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                Log.e(TAG, "setUserVisibleHint: ", e);
            }
        }
    }


    // --------------------------------- [ End of SearView all query methods ] -------------------\\

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
    @Override
    public void onStart() {
        super.onStart();
    }

}



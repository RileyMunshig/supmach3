package in.munshig.supmach3.views.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.tabs.TabLayout;
import java.util.Objects;
import in.munshig.supmach3.R;
import in.munshig.supmach3.adapter.ProfileAdapter;
import in.munshig.supmach3.utils.InputUtil;
import in.munshig.supmach3.views.profile.ForecastF;
import in.munshig.supmach3.views.profile.InsightsF;
import in.munshig.supmach3.views.profile.PersonalF;
import in.munshig.supmach3.views.profile.ProfileF;
import in.munshig.supmach3.views.profile.SettingsF;
public class ProfileFragment extends Fragment {
    public ProfileFragment() {
    }

    /**
     * instance of ProfileFragment fragment.
     * @return A new instance of fragment ProfileFragment.
     */
    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragments_insights,container, false);
        ViewPager viewPager = view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        TabLayout profile_tabs = view.findViewById(R.id.result_tabs);
        profile_tabs.setupWithViewPager(viewPager);
        BottomAppBar appBarLayout = view.findViewById(R.id.appBar);
/*
        profile_tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 4) {
                    //TODO:Collapse pager
                  appBarLayout.isLiftOnScroll();
                }
                else{

                    //TODO:

                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });*/
        return  view;

    }
    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InputUtil.hideKeyboard(Objects.requireNonNull(getContext()), view);

    }
        // inner Viewpager
        private void setupViewPager(ViewPager viewPager) {
            ProfileAdapter adapter = new ProfileAdapter(getChildFragmentManager());
            adapter.addFragment(new InsightsF(), "Insights");
            adapter.addFragment(new ProfileF(), "Profile");
            adapter.addFragment(new SettingsF(), "Settings");
            adapter.addFragment(new ForecastF(), "Forecast");
            adapter.addFragment(new PersonalF(),"Personal");
            viewPager.setAdapter(adapter);
        }





}

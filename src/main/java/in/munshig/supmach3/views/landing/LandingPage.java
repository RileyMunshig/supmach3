package in.munshig.supmach3.views.landing;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.franmontiel.localechanger.LocaleChanger;
import com.franmontiel.localechanger.utils.ActivityRecreationHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jakewharton.rxbinding3.view.RxView;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.TimeUnit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.munshig.supmach3.R;
import in.munshig.supmach3.utils.Pref;
import in.munshig.supmach3.utils.ViewUtils;
import in.munshig.supmach3.views.anim.BounceAnim;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import timber.log.Timber;
import static in.munshig.supmach3.base.Persist.SUPPORTED_LOCALES;
public class LandingPage extends ViewUtils {
    private static final String TAG = LandingPage.class.getSimpleName();
    private ViewPager viewPager;
    private TextView[] dots;
    private int[] layouts;
    @BindView(R.id.btn_skip) public Button btnSkip;
    @BindView(R.id.btn_next) public Button btnNext;
   // @BindView(R.id.sup) public TextView supTv;
    private Pref prefManager;
    int langSelected = Pref.LANGUAGE_ENGLISH;
    LinearLayout dotsLayout;
    Animation anim;
    Unbinder unbinder;
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if (position == layouts.length - 1) {
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            } else {
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }
        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
       unbinder = ButterKnife.bind(this);
        prefManager = new Pref(this);
        if (!prefManager.isFirstTimeLaunch()) {
            goHome();
            finish();
        }
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        showLangDialog(this);
        Log.d(TAG, "onCreate: ");
        dotsLayout = findViewById(R.id.layoutDots);
        viewPager = findViewById(R.id.view_pager);
        btnSkip = findViewById(R.id.btn_skip);
        btnNext = findViewById(R.id.btn_next);
        layouts = new int[]{R.layout.slide1, R.layout.slide2, R.layout.slide3, R.layout.slide4};
        addBottomDots(0);
        changeStatusBarColor();
        LandingPageViewPager myViewPagerAdapter = new LandingPageViewPager();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        RxView.clicks(btnSkip).throttleFirst(2, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) {

                        goHome();
                    }
                });


        RxView.clicks(btnNext).throttleFirst(2, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe((Consumer<Object>) o -> {
                    int current = getItem(+1);
                    if (current < layouts.length) {
                        // move to next screen
                        viewPager.setCurrentItem(current);
                    } else {
                        finish();

                    }
                });

        RxView.clicks(btnSkip).throttleFirst(2, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe((Consumer<Object>) o -> {
                    int current = getItem(+1);
                    finish();
                });
    }



    //----LaNguage Dialog[END]--------------------------------------------------------------------//

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }
        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    /**
     * Making notification bar transparent
     */

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //----VIEWPAGER [START]--------------------------------------------------------------------//
    public class LandingPageViewPager extends PagerAdapter {
        private LayoutInflater layoutInflater;
        LandingPageViewPager() {}


        @NotNull
        @Override
        public Object instantiateItem(@NotNull ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }
        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NotNull View view, @NotNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(@NotNull ViewGroup container, int position, @NotNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
    private void showLangDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_language);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setDialogViews(dialog);
        dialog.show();
    }
    private void setDialogViews(Dialog dialog) {
        RelativeLayout englishL, hindiL, hinglishL, malayalamL;
        englishL = dialog.findViewById(R.id.englishLayout);
        hindiL = dialog.findViewById(R.id.hindiLanguage);
        hinglishL = dialog.findViewById(R.id.hinglishLanguage);
        malayalamL = dialog.findViewById(R.id.malayalamLanguage);

        ImageView englishIcon, hindiIcon, hinglishIcon, malayalamIcon;
        englishIcon = dialog.findViewById(R.id.englishCheck);
        hindiIcon = dialog.findViewById(R.id.hindiCheck);
        hinglishIcon = dialog.findViewById(R.id.hinglishCheck);
        malayalamIcon = dialog.findViewById(R.id.malayalamCheck);
        FloatingActionButton showLang = findViewById(R.id.show_lang);

        //Current Language is :D
        int currentL = prefManager.getCurrentLanguage();
        if (currentL == Pref.LANGUAGE_ENGLISH) {
            hideViews(hindiIcon, hinglishIcon, malayalamIcon);
            showViews(englishIcon);
        } else if (currentL == Pref.LANGUAGE_HINDI) {
            hideViews(englishIcon, hinglishIcon, malayalamIcon);
            showViews(hindiIcon);
        } else if (currentL == Pref.LANGUAGE_HINGLISH) {
            hideViews(englishIcon, hindiIcon, malayalamIcon);
            showViews(hinglishIcon);
        } else {
            hideViews(englishIcon, hindiIcon, hinglishIcon);
            showViews(malayalamIcon);
        }
        englishL.setOnClickListener(v -> {
            hideViews(hindiIcon, hinglishIcon, malayalamIcon);
            showViews(englishIcon);
            langSelected = Pref.LANGUAGE_ENGLISH;
        });

        hindiL.setOnClickListener(v -> {
            hideViews(englishIcon, hinglishIcon, malayalamIcon);
            showViews(hindiIcon);
            langSelected = Pref.LANGUAGE_HINDI;
        });

        hinglishL.setOnClickListener(v -> {
            hideViews(englishIcon, hindiIcon, malayalamIcon);
            showViews(hinglishIcon);
            langSelected = Pref.LANGUAGE_HINGLISH;
        });
        malayalamL.setOnClickListener(v -> {
            hideViews(englishIcon, hindiIcon, hinglishIcon);
            showViews(malayalamIcon);
            langSelected = Pref.LANGUAGE_MALAYALAM;
        });

        Button save = dialog.findViewById(R.id.saveBtn);
        save.setOnClickListener(v -> {
            Toast.makeText(LandingPage.this, "Language Changed", Toast.LENGTH_LONG).show();
            prefManager.setCurrentLanguage(langSelected);
            LocaleChanger.setLocale(SUPPORTED_LOCALES.get(prefManager.getCurrentLanguage()));
            ActivityRecreationHelper.recreate(LandingPage.this, true);
            Log.i("GetInfo", " || " + SUPPORTED_LOCALES.get(prefManager.getCurrentLanguage()));
            dialog.cancel();
            showLang.show();
            //dialog.dismiss();

        });
        Button cancel = dialog.findViewById(R.id.cancelBtn);
        cancel.setOnClickListener(v -> {
            Toast.makeText(getApplicationContext(), "canceled", Toast.LENGTH_SHORT).show();
            dialog.cancel();
            showLang.show();
        });
        //OnClick listener for floating action button
        showLang.setOnClickListener(v->{
            final Animation myAnim = AnimationUtils.loadAnimation(LandingPage.this, R.anim.bounce);
            BounceAnim bounceAnim = new BounceAnim(0.2, 20);
            myAnim.setInterpolator(bounceAnim);
            showLang.startAnimation(myAnim);
            dialog.show();
        });
    }

    //----VIEWPAGER Dialog[END]--------------------------------------------------------------------//

    @Override
    protected void onStart() {
        super.onStart();
        Timber.d("onStart");
    }


    @Override
    protected void onResume() {
        super.onResume();
        ActivityRecreationHelper.onResume(this);
        Timber.d("onResume");
    }

    @Override
    protected void onDestroy() {
        ActivityRecreationHelper.onDestroy(this);
        Timber.d("onDestroy");
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Timber.d("onRestart");
    }
}

package in.munshig.supmach3.views.modals;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
@Entity
public class User implements Serializable {
    @NonNull
    @PrimaryKey
    private String uid;
    private String name;
    private String number;
    private String chatRef;
    private long created, modified;
    private double netBalance;
    private String avatarUri;

    public User(){}

    @Ignore
    public User(@NonNull String uid, String name, String number, String chatRef, long created, long modified, double netBalance, String avatarUri) {
        this.uid = uid;
        this.name = name;
        this.number = number;
        this.chatRef = chatRef;
        this.created = created;
        this.modified = modified;
        this.netBalance = netBalance;
        this.avatarUri = avatarUri;
    }

    @Ignore
    public User(@NotNull String cId, String name, String number) {
        this.name = name;
        this.number = number;
        this.uid = cId;
    }

    @Ignore
    public User(@NotNull String cId, String name) {
        this.name = name;
        this.uid = cId;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public double getNetBalance() {
        return netBalance;
    }

    public void setNetBalance(double netBalance) {
        this.netBalance = netBalance;
    }


    @NotNull
    public String getUid() {
        return uid;
    }

    public void setUid(@NotNull String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getChatRef() {
        return chatRef;
    }

    public void setChatRef(String docRef) {
        this.chatRef = docRef;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    @NotNull
    @Override
    public String toString() {
        return "User{" +
                "uid ='" + uid + '\'' +
                ", name ='" + name + '\'' +
                ", number ='" + number + '\'' +
                ", chatRef ='" + chatRef + '\'' +
                ", created ='" + created + '\'' +
                ", modified='" + modified + '\'' +
                '}';
    }
}



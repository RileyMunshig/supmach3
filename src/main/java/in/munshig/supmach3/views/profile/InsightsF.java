package in.munshig.supmach3.views.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.munshig.supmach3.R;
import in.munshig.supmach3.utils.InputUtil;

public class InsightsF extends Fragment {
        public InsightsF() {
            // Required empty public constructor
        }
        /**
         * Create a new instance of this fragment
         *
         * @return A new instance of fragment LogFragment.
         */
        public static InsightsF newInstance() {
            return new InsightsF();
        }

        @Override
        public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            super.onCreateView(inflater, container, savedInstanceState);
            View view1 = inflater.inflate(R.layout.insightsf, container, false);

            AnyChartView anyChartView = view1.findViewById(R.id.any_chart_view);
            anyChartView.setProgressBar(view1.findViewById(R.id.progress_bar));
            Pie pie = AnyChart.pie();
            pie.credits().logoSrc();

            pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
                @Override
                public void onClick(Event event) {
                    Toast.makeText(getActivity(), event.getData().get("x") + ":" + event.getData().get("value"), Toast.LENGTH_SHORT).show();
                }
            });
            List<DataEntry> data = new ArrayList<>();
            data.add(new ValueDataEntry("Sales", 6371664));
            data.add(new ValueDataEntry("Udhar", 789622));
            data.add(new ValueDataEntry("Purchases", 7216301));
            data.add(new ValueDataEntry("Expenses", 7216301));
            pie.data(data);

            pie.title("Insights on your revenue and expenses");

            pie.labels().position("outside");

            pie.legend().title().enabled(true);
            pie.legend().title()
                    .text("Index")
                    .padding(0d, 0d, 10d, 0d);

            pie.legend()
                    .position("center-bottom")
                    .itemsLayout(LegendLayout.HORIZONTAL)
                    .align(Align.CENTER);

            anyChartView.setChart(pie);
            //anyChartView.setLicenceKey();


            // Inflate the appbar_layout for this fragment
           return  view1;
        }
        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            InputUtil.hideKeyboard(Objects.requireNonNull(getContext()), view);

        }
    }


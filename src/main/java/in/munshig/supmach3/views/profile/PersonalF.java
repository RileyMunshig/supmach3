package in.munshig.supmach3.views.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

import butterknife.BindView;
import in.munshig.supmach3.R;
import in.munshig.supmach3.utils.InputUtil;
import ru.whalemare.sheetmenu.SheetMenu;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

public class PersonalF extends Fragment {

    public PersonalF() {
        // Required empty public constructor
    }
    private GridLayout dashboard;
    BottomAppBar bar;
    /**
     * Create a new instance of this fragment
     *
     * @return A new instance of fragment PersonalF.
     */
    public static PersonalF newInstance() {
        return new PersonalF();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personalf,container, false);
        dashboard = view.findViewById(R.id.mainGrid);
        setSingleEvent(dashboard);

        //BottomNavigationView bottomNavigationView = view.findViewById(R.id.bottomNavigation);
        bar = view. findViewById(R.id.bar);
        bar.replaceMenu(R.menu.insurance_menu);
        bar.setNavigationOnClickListener(v -> {
            // Handle the navigation click by showing a BottomDrawer etc.
        });

    /*    bottomNavigationView.setOnNavigationItemSelectedListener
                (item -> {
                    switch (item.getItemId()) {
                        case R.id.home:
                            Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_LONG).show();
                            break;
                        case R.id.trending:
                            Toast.makeText(getApplicationContext(),"Trending",Toast.LENGTH_LONG).show();

                            break;
                        case R.id.bookmarks:
                            Toast.makeText(getApplicationContext(),"Bookmarks",Toast.LENGTH_LONG).show();

                            break;
                        case R.id.activity:
                            Toast.makeText(getApplicationContext(),"Activity",Toast.LENGTH_LONG).show();

                            break;
                    }
                    return false;
                });*/

        return  view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InputUtil.hideKeyboard(Objects.requireNonNull(getContext()), view);
    }

    private void setSingleEvent(GridLayout gridLayout) {
        for(int i = 0; i<gridLayout.getChildCount();i++){
            CardView cardView = (CardView)gridLayout.getChildAt(i);
            final int finalI= i;
            cardView.setOnClickListener(view -> {
             //TODO:
            });

            //showMenu
         // cardView.setOnClickListener(view -> showMenu());
          cardView.setOnClickListener(view -> Toast.makeText(getActivity(),"Clicked at index "+ finalI,
                   Toast.LENGTH_SHORT).show());
        }
    }
    private void showMenu() {
        SheetMenu.with(getContext())
                .setTitle("Select option:").
                setMenu(R.menu.sheet_menu)
                .setClick(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getItemId() == R.id.action_atom)
                            Toast.makeText(getActivity(), "Atom action", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(getActivity(), "Disco action", Toast.LENGTH_LONG).show();

                        return false;
                    }
                }).show();
    }

}


package in.munshig.supmach3.views.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import in.munshig.supmach3.R;
import in.munshig.supmach3.utils.InputUtil;
import timber.log.Timber;

public class ProfileF extends Fragment {
    public ProfileF() {
        // Required empty public constructor
    }


    /**
     * Create a new instance of this fragment
     *
     * @return A new instance of fragment LogFragment.
     */
    public static ProfileF newInstance() {
        return new ProfileF();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the appbar_layout for this fragment
        return inflater.inflate(R.layout.profilef,container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InputUtil.hideKeyboard(Objects.requireNonNull(getContext()), view);

    }

    @Override
    public void onStart() {
        super.onStart();

        String User = FirebaseAuth.getInstance().getUid();
        Timber.d("hello" + User);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
//setSupportActionBar(bottom_appbar) calling this breaks it!
    // setting the menu

}




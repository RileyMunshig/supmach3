package in.munshig.supmach3.views.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import in.munshig.supmach3.R;
import in.munshig.supmach3.utils.InputUtil;

public class SettingsF extends Fragment {
    public SettingsF() {
        // Required empty public constructor
    }
    /**
     * Create a new instance of this fragment
     *
     * @return A new instance of fragment LogFragment.
     */
    public static SettingsF newInstance() {
        return new SettingsF();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the appbar_layout for this fragment
        return inflater.inflate(R.layout.settingsf, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InputUtil.hideKeyboard(Objects.requireNonNull(getContext()), view);

    }
}


package `in`.munshig.sup2.view.activities

import `in`.munshig.supmach3.views.activities.HomeActivity
import `in`.munshig.supmach3.R
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Splash : AppCompatActivity() {
    internal lateinit var splashTread: Thread
    override fun onAttachedToWindow(){
       /* override fun attachBaseContext(newBase: Context) {
            var newBase = newBase
            newBase = LocaleChanger.configureBaseContext(newBase)
            super.attachBaseContext(newBase)
        }

*/
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        StartSplash()
    }
    private fun StartSplash() {
        val text_anim = AnimationUtils.loadAnimation(this, R.anim.translate)
        val txt = findViewById<TextView>(R.id.sup)
        txt.clearAnimation()
        txt.startAnimation(text_anim)


        val anim = AnimationUtils.loadAnimation(this, R.anim.translate)
        anim.reset()
        val l = findViewById<ImageView>(R.id.splash_img)
        l.clearAnimation()
        l.startAnimation(anim)

        /* Start thread */
        splashTread = object : Thread() {
            override fun run() {
                try {
                    var waited = 0
                    // Splash screen pause time
                    while (waited < 3500) {
                        Thread.sleep(100)
                        waited += 100
                    }
                    val intent = Intent(this@Splash,
                            HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                    startActivity(intent)
                    this@Splash.finish()

                } catch (e: InterruptedException) {
                    // do nothing
                } finally {
                    this@Splash.finish()

                }

            }
        }
        splashTread.start()
    }
}
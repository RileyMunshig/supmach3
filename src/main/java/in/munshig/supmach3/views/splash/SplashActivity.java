package in.munshig.supmach3.views.splash;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import org.reactivestreams.Subscription;
import java.util.concurrent.TimeUnit;
import in.munshig.supmach3.views.activities.HomeActivity;
import in.munshig.supmach3.R;
import in.munshig.supmach3.views.authui.AuthActivity;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SplashActivity extends AppCompatActivity implements SplashNavigator {
    private Subscription subscription;
    SplashNavigator splashNavigator;
    Observer observer = new Observer() {
        @Override
        public void onError(Throwable e) {
            Toast.makeText(SplashActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onComplete() {
            finish();
        }
        @Override
        public void onSubscribe(Disposable d) {}

        @Override
        public void onNext(Object o) {
            startActivity(new Intent(SplashActivity.this, AuthActivity.class));
            finish();}
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startAnimation();
    }
    private void startAnimation() {
        Animation anim;
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = findViewById(R.id.splash_img);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }
    @Override
    public void onBackPressed() {
        subscription.cancel();
        //subscription.unsubscribe();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
       // subscription.pause();
       //subscription.unsubscribe();
        super.onPause();
    }

    @Override
    protected void onResume() {
        //subscription = Observable.timer(3,TimeUnit.SECONDS).subscribe(subscription);
        Observable.timer(3, TimeUnit.SECONDS).subscribe(observer);
        super.onResume();
    }

    @Override
    public void openLoginActivity() {
        splashNavigator.openLoginActivity();

    }

    @Override
    public void openMainActivity() {
        splashNavigator.openMainActivity();

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

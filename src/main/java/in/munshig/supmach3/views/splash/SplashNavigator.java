package in.munshig.supmach3.views.splash;
public interface SplashNavigator {

    void openLoginActivity();

    void openMainActivity();

}

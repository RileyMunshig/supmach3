package in.munshig.supmach3.views.transactions;


import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

import in.munshig.supmach3.R;
import in.munshig.supmach3.utils.Dinosaur;
import in.munshig.supmach3.utils.ViewUtils;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

//TODO:Transaction Chat
public class Transactions extends ViewUtils {
    private TextView txtName;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        txtName = findViewById(R.id.textViewName);
        Dinosaur.getInstance().listen().subscribe(getInputObserver());
    }
    // Get input observer instance
    private Observer<String> getInputObserver() {
        return new Observer<String>() {
            @Override public void onSubscribe(Disposable d) {
            }
            @Override public void onNext(String s) {
                txtName.setText(s);
            }
            @Override public void onError(Throwable e) {
            }
            @Override public void onComplete() {
            }
        };
    }
}
